<?php

declare(strict_types=1);

namespace asmaru\json;

/**
 * Interface JsonUnserializable
 *
 * @package asmaru\json
 */
interface JsonUnserializable {

	/**
	 * @param $data
	 * @return mixed
	 */
	public static function jsonUnserialize($data);
}