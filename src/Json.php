<?php

declare(strict_types=1);

namespace asmaru\json;

use function json_decode;
use function json_encode;
use const JSON_PRETTY_PRINT;

/**
 * Class Json
 *
 * @package asmaru\json
 */
class Json {

	/**
	 * @param string $data
	 * @return mixed
	 */
	public static function fromString(string $data) {
		return json_decode($data, true);
	}

	/**
	 * @param $array
	 * @return string
	 */
	public static function toString($array): string {
		return json_encode($array, JSON_PRETTY_PRINT);
	}
}